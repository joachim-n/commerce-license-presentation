# Sell it again (and again)

## Commerce License & Commerce Recurring

---

## Joachim Noreiko

Drupal contributor for 10+ years.

- Flag
- Module Builder
- Far too many other contrib modules

### Contact

- https://www.drupal.org/u/joachim
- https://twitter.com/joachimnicolas
- IRC - joachim-n
- https://drupal.slack.com/ - joachim

---

# Commerce License & Commerce Recurring

- https://www.drupal.org/project/commerce_license
- https://www.drupal.org/project/commerce_recurring

---

# What can you sell with a license?

@ul

- Roles
- OG Group membership
- Entity fields
  - Published
  - Promoted
  - ...etc
- ... your idea here?

@ulend

---

@snap[north]

<h1>Components</h1>

@snapend

## License entity:

- Content entity
- Owned by the user
- Has state: active / expired / cancelled, etc
- Bundle is the license type

---

@transition[none]

@snap[north]

<h1>Components</h1>

@snapend

## License type plugin:

- Grants access
- Revokes access
- Defines the bundles of the License entity

---

@transition[none]

@snap[north]

<h1>Components</h1>

@snapend

## Recurring period plugin:

- Determines an end date given a start date
- Different plugins:
  - Unlimited: never ends
  - Rolling period: e.g. 1 year from purchase date
  - Fixed period: e.g. until January 1

---?image=images/patch-cables.jpg

# @color[white](Setup)

@css[photo-credit](Photo credit: https://www.flickr.com/photos/retrocactus/24386131611)

---

## Configuration chain: standard

- Product type
  - → Product variation type
    - → Order item type
      - → Order type
        - → Checkout flow
        - → Order workflow

---

## Configuration chain: licenses

- Product type
  - → Product variation type - License trait
    - → Order item type - License trait
      - → Order type
        - → Checkout flow
          - No anonymous checkout
        - → Order workflow

---

## Entity traits

*Entity traits* are plugins which are set on a bundle, and provide pluggable behaviour such as fields.

- Product variation type
  - License type plugin configuration
- Order item type
  - License entity reference

---

### Product variation type

![Product variation type configuration](images/product-variation-type-role.png)

---

### Order item type

![Order item type configuration](images/order-item-type-license.png)

---

## Product configuration

![Product variation for role license](images/product-variation-role.png)

---

## Purchase product

- Put in cart.
- Go through checkout.
- Place order.
  - License created.
  - License optionally activated.
- Order is validated, if applicable.
- License activated.

---

![Rubber stamp](images/rubber-stamp.jpg)

@css[photo-credit](Photo credit: https://www.flickr.com/photos/56271618@N00/4300566866)

---

## Role license

![Role license](images/license-role.png)

---


expiry

- unlimited
- rolling
- fixed
- ... other cases: custom plugins

---
 expiry: cron run!

 - email notification

---

Order sync

- cancel orders
- place / validation / completion

---

## Constraints

- Can't buy multiples of a license products
- Can't buy license product that grants something you already have
expand on both of these!

# Other license plugins

- entity field
- OG group




......

---

## Contribution day

Lots to do on Commerce License and Recurring!

- Checkout pane for license details
- User UI for subscriptions and licenses
- Admin UI improvements for License and Recurring
- New license types

